﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Appium.Android;
using NUnit.Framework;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Remote;
using System;
using Android_Test.Pages;
using OpenQA.Selenium.Appium.Enums;
using OpenQA.Selenium;
using Android_Test.Besiness;

namespace Android_Test
{
    [TestClass]
    public class Calculator
    {
        AndroidDriver<AndroidElement> driver;
        Keyboard keyboard;
      
        [TestInitialize]
        public void Init()
        {
            ConnectionTotheDevice connection = new ConnectionTotheDevice();
            driver = connection.GetDriver();
            keyboard = new Keyboard();
        }

        [TestCleanup]
        public void Cleanup()
        {
            driver.CloseApp();
            driver.Quit();
        }

        [TestMethod]
        [DataRow("10 + 2","12")]
        [DataRow("10 - 2", "8")]
        [DataRow("10 ÷ 2", "5")]
        [DataRow("10 × 2", "20")]
        public void BasicMathematicalOperations(string Value, string expectedValue)
        {
            string result = keyboard.MathematicalOperations(driver, Value);

            keyboard.Keyboard_Assert(result, expectedValue);
        }

        [TestMethod]
        [DataRow("1", "1")]
        [DataRow("2", "2")]
        [DataRow("3", "3")]
        [DataRow("4", "4")]
        [DataRow("5", "5")]
        [DataRow("6", "6")]
        [DataRow("7", "7")]
        [DataRow("8", "8")]
        [DataRow("9", "9")]
        [DataRow("0", "0")]
        public void CheckTheNumberButtons(string Value,string expectedValue)
        {
            string result = keyboard.TheNumberButtons(driver, Value);

            keyboard.Keyboard_Assert(result, expectedValue);
        }

        [TestMethod]
        [DataRow("10", "2", "add", "10+2")]
        [DataRow("10", "2", "sub", "10−2")]
        [DataRow("10", "2", "mul", "10×2")]
        [DataRow("10", "2", "div", "10÷2")]
        public void CheckTheButtonsOfMathematicalOperations(string firstValue, string secondValue, string operation, string expectedValue)
        {
            string result = keyboard.TheButtonsOfMathematicalOperations(driver, firstValue,secondValue,operation);

            keyboard.Keyboard_Assert(result, expectedValue);
        }

        [TestMethod]
        [DataRow("3", "(−3")]
        [DataRow("(-3", "3")]
        public void CheckPlusMinus(string Value, string expectedValue)
        {
            string result = keyboard.PlusMinus(driver, Value);

            keyboard.Keyboard_Assert(result, expectedValue);
        }

        [TestMethod]
        [DataRow("0,")]
        public void CheckDot( string expectedValue)
        {
            string result = keyboard.Dot(driver);

            keyboard.Keyboard_Assert(result, expectedValue);
        }

        [TestMethod]
        [DataRow("(")]
        public void CheckParenthesis(string expectedValue)
        {
            string result = keyboard.Parenthesis(driver);

            keyboard.Keyboard_Assert(result, expectedValue);
        }

        [TestMethod]
        [DataRow("5","0,05")]
        public void CheckPercentage(string Value,string expectedValue)
        {
            string result = keyboard.Percentage(driver,Value);

            keyboard.Keyboard_Assert(result, expectedValue);
        }

        [TestMethod]
        [DataRow("12345", "")]
        public void CheckClear(string Value, string expectedValue)
        {
            string result1 = keyboard.Clear_EditFormula(driver, Value);
            string result2 = keyboard.Clear_Result(driver, Value);

            keyboard.Keyboard_Assert(result1, result2, expectedValue);
        }

        [TestMethod]
        [DataRow("1234", "123")]
        public void CheckDelete(string Value, string expectedValue)
        {
            string result = keyboard.Delete(driver, Value);

            keyboard.Keyboard_Assert(result, expectedValue);
        }

        [TestMethod]
        [DataRow("1+1", "2")]
        public void CheckEqual(string Value, string expectedValue)
        {
            string result = keyboard.Equal(driver, Value);

            keyboard.Keyboard_Assert(result, expectedValue);
        }
    }
}