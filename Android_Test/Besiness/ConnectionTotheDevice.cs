﻿using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.Enums;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Android_Test.Besiness
{
    class ConnectionTotheDevice
    {
        public DesiredCapabilities DesiredCapabilities() 
        {
            DesiredCapabilities desired = new DesiredCapabilities();
            desired.SetCapability(MobileCapabilityType.PlatformName, "Android");
            desired.SetCapability("appium:deviceName", "RZ8M81JG7PK");
            desired.SetCapability("appium:appPackage", "com.sec.android.app.popupcalculator");
            desired.SetCapability("appium:appActivity", "com.sec.android.app.popupcalculator.Calculator");
            return desired;
        }
        public AndroidDriver<AndroidElement> GetDriver() 
        {
            Uri url = new Uri("http://127.0.0.1:4723/wd/hub");
            return new AndroidDriver<AndroidElement>(url, DesiredCapabilities());
        }

        
    }
}
