﻿using Android_Test.Pages;
using OpenQA.Selenium.Appium.Android;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Android_Test.Besiness
{
    class Keyboard
    {
        public string MathematicalOperations(AndroidDriver<AndroidElement> driver,string Value) 
        {
            CalculationPage calculation = new CalculationPage(driver);
            calculation.SendKeysTo_EditFormulaTextBox(Value);
            return calculation.Text_ResultTextBox();
        }
        public string TheNumberButtons(AndroidDriver<AndroidElement> driver, string Value)
        {
            CalculationPage calculation = new CalculationPage(driver);

            if (Value == "0") 
            {
                calculation.ClickOn_keypadButton0();
            }
            if (Value == "1")
            {
                calculation.ClickOn_keypadButton1();
            }
            if (Value == "2")
            {
                calculation.ClickOn_keypadButton2();
            }
            if (Value == "3")
            {
                calculation.ClickOn_keypadButton3();
            }
            if (Value == "4")
            {
                calculation.ClickOn_keypadButton4();
            }
            if (Value == "5")
            {
                calculation.ClickOn_keypadButton5();
            }
            if (Value == "6")
            {
                calculation.ClickOn_keypadButton6();
            }
            if (Value == "7")
            {
                calculation.ClickOn_keypadButton7();
            }
            if (Value == "8")
            {
                calculation.ClickOn_keypadButton8();
            }
            if (Value == "9")
            {
                calculation.ClickOn_keypadButton9();
            }



            return calculation.Text_EditFormulaTextBox();
        }

        public string TheButtonsOfMathematicalOperations(AndroidDriver<AndroidElement> driver, string firstValue, string secondValue, string operation)
        {
            CalculationPage calculation = new CalculationPage(driver);

            calculation.SendKeysTo_EditFormulaTextBox(firstValue);

            if (operation == "add")
            {
                calculation.ClickOn_keypadButtonAdd();
            }
            if (operation == "sub")
            {
                calculation.ClickOn_keypadButtonSub();
            }
            if (operation == "mul")
            {
                calculation.ClickOn_keypadButtonMul();
            }
            if (operation == "div")
            {
                calculation.ClickOn_keypadButtonDiv();
            }

            calculation.SendKeysTo_EditFormulaTextBox(secondValue);

            return calculation.Text_EditFormulaTextBox();
        }

        public string PlusMinus(AndroidDriver<AndroidElement> driver, string Value)
        {
            CalculationPage calculation = new CalculationPage(driver);
            calculation.SendKeysTo_EditFormulaTextBox(Value);
            calculation.ClickOn_keypadButtonPlusMinus();

            return calculation.Text_EditFormulaTextBox();
        }

        public string Dot(AndroidDriver<AndroidElement> driver)
        {
            CalculationPage calculation = new CalculationPage(driver);
            calculation.ClickOn_keypadButtonDot();

            return calculation.Text_EditFormulaTextBox();
        }

        public string Parenthesis(AndroidDriver<AndroidElement> driver)
        {
            CalculationPage calculation = new CalculationPage(driver);
            calculation.ClickOn_keypadParenthesis();

            return calculation.Text_EditFormulaTextBox();
        }

        public string Percentage(AndroidDriver<AndroidElement> driver, string Value)
        {
            CalculationPage calculation = new CalculationPage(driver);
            calculation.SendKeysTo_EditFormulaTextBox(Value);
            calculation.ClickOn_keypadPercentage();

            return calculation.Text_ResultTextBox();
        }

        public string Clear_EditFormula(AndroidDriver<AndroidElement> driver, string Value)
        {
            CalculationPage calculation = new CalculationPage(driver);
            calculation.SendKeysTo_EditFormulaTextBox(Value);
            calculation.ClickOn_keypadСlear();

            return calculation.Text_EditFormulaTextBox();
        }
        public string Clear_Result(AndroidDriver<AndroidElement> driver, string Value)
        {
            CalculationPage calculation = new CalculationPage(driver);
            calculation.SendKeysTo_EditFormulaTextBox(Value);
            calculation.ClickOn_keypadСlear();

            return calculation.Text_ResultTextBox();
        }

        public string Delete(AndroidDriver<AndroidElement> driver, string Value)
        {
            CalculationPage calculation = new CalculationPage(driver);
            calculation.SendKeysTo_EditFormulaTextBox(Value);
            calculation.ClickOn_keypadDelete();

            return calculation.Text_EditFormulaTextBox();
        }

        public string Equal(AndroidDriver<AndroidElement> driver, string Value)
        {
            CalculationPage calculation = new CalculationPage(driver);
            calculation.SendKeysTo_EditFormulaTextBox(Value);
            calculation.ClickOn_keypadEqual();

            return calculation.Text_EditFormulaTextBox();
        }

        public void Keyboard_Assert(string Value1, string Value2)
        {
            NUnit.Framework.Assert.IsTrue(Value1== Value2);
        }
        public void Keyboard_Assert(string Value11, string Value12, string Value2)
        {
            NUnit.Framework.Assert.IsTrue(Value11 == Value2&& Value12 == Value2);
        }
    }
}
