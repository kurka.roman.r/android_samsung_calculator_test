﻿using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Remote;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Android_Test.Pages
{
    class BasePage
    {
        public  AndroidDriver<AndroidElement> driver;
        public BasePage(AndroidDriver<AndroidElement> _driver) 
        {
            driver = _driver;
            PageFactory.InitElements(driver,this);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3);
        }
    }
}
