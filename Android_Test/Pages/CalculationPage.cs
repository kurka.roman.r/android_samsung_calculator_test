﻿using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Remote;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Android_Test.Pages
{
    class CalculationPage:BasePage
    {
        public CalculationPage(AndroidDriver<AndroidElement> _driver) : base(_driver) { }

        [FindsBy(How = How.Id, Using = "com.sec.android.app.popupcalculator:id/calc_edt_formula")]
        private IWebElement editFormulaTextBox;
        [FindsBy(How = How.Id, Using = "com.sec.android.app.popupcalculator:id/calc_tv_result")]
        private IWebElement resultTextBox;

        [FindsBy(How = How.Id, Using = "com.sec.android.app.popupcalculator:id/calc_keypad_btn_00")]
        private IWebElement keypadButton0;
        [FindsBy(How = How.Id, Using = "com.sec.android.app.popupcalculator:id/calc_keypad_btn_01")]
        private IWebElement keypadButton1;
        [FindsBy(How = How.Id, Using = "com.sec.android.app.popupcalculator:id/calc_keypad_btn_02")]
        private IWebElement keypadButton2;
        [FindsBy(How = How.Id, Using = "com.sec.android.app.popupcalculator:id/calc_keypad_btn_03")]
        private IWebElement keypadButton3;
        [FindsBy(How = How.Id, Using = "com.sec.android.app.popupcalculator:id/calc_keypad_btn_04")]
        private IWebElement keypadButton4;
        [FindsBy(How = How.Id, Using = "com.sec.android.app.popupcalculator:id/calc_keypad_btn_05")]
        private IWebElement keypadButton5;
        [FindsBy(How = How.Id, Using = "com.sec.android.app.popupcalculator:id/calc_keypad_btn_06")]
        private IWebElement keypadButton6;
        [FindsBy(How = How.Id, Using = "com.sec.android.app.popupcalculator:id/calc_keypad_btn_07")]
        private IWebElement keypadButton7;
        [FindsBy(How = How.Id, Using = "com.sec.android.app.popupcalculator:id/calc_keypad_btn_08")]
        private IWebElement keypadButton8;
        [FindsBy(How = How.Id, Using = "com.sec.android.app.popupcalculator:id/calc_keypad_btn_09")]
        private IWebElement keypadButton9;

        [FindsBy(How = How.Id, Using = "com.sec.android.app.popupcalculator:id/calc_keypad_btn_add")]
        private IWebElement keypadButtonAdd;
        [FindsBy(How = How.Id, Using = "com.sec.android.app.popupcalculator:id/calc_keypad_btn_sub")]
        private IWebElement keypadButtonSub;
        [FindsBy(How = How.Id, Using = "com.sec.android.app.popupcalculator:id/calc_keypad_btn_mul")]
        private IWebElement keypadButtonMul;
        [FindsBy(How = How.Id, Using = "com.sec.android.app.popupcalculator:id/calc_keypad_btn_div")]
        private IWebElement keypadButtonDiv;

        [FindsBy(How = How.Id, Using = "com.sec.android.app.popupcalculator:id/calc_keypad_btn_plusminus")]
        private IWebElement keypadButtonPlusminus;

        [FindsBy(How = How.Id, Using = "com.sec.android.app.popupcalculator:id/calc_keypad_btn_dot")]
        private IWebElement keypadButtonDot;

        [FindsBy(How = How.Id, Using = "com.sec.android.app.popupcalculator:id/calc_keypad_btn_parenthesis")]
        private IWebElement keypadButtonParenthesis;

        [FindsBy(How = How.Id, Using = "com.sec.android.app.popupcalculator:id/calc_keypad_btn_percentage")]
        private IWebElement keypadButtonPercentage;

        [FindsBy(How = How.Id, Using = "com.sec.android.app.popupcalculator:id/calc_keypad_btn_clear")]
        private IWebElement keypadButtonСlear;

        [FindsBy(How = How.Id, Using = "com.sec.android.app.popupcalculator:id/calc_handle_btn_delete")]
        private IWebElement keypadButtonDelete;

        [FindsBy(How = How.Id, Using = "com.sec.android.app.popupcalculator:id/calc_keypad_btn_equal")]
        private IWebElement keypadButtonEqual;

        public void SendKeysTo_EditFormulaTextBox(string text) 
        {
            editFormulaTextBox.SendKeys(text);
        }
        public string Text_EditFormulaTextBox()
        {
            return editFormulaTextBox.Text;
        }
        public string Text_ResultTextBox() 
        {
            return resultTextBox.Text;
        }


        public void ClickOn_keypadButton0()
        {
            keypadButton0.Click();
        }
        public void ClickOn_keypadButton1()
        {
            keypadButton1.Click();
        }
        public void ClickOn_keypadButton2()
        {
            keypadButton2.Click();
        }
        public void ClickOn_keypadButton3()
        {
            keypadButton3.Click();
        }
        public void ClickOn_keypadButton4()
        {
            keypadButton4.Click();
        }
        public void ClickOn_keypadButton5()
        {
            keypadButton5.Click();
        }
        public void ClickOn_keypadButton6()
        {
            keypadButton6.Click();
        }
        public void ClickOn_keypadButton7()
        {
            keypadButton7.Click();
        }
        public void ClickOn_keypadButton8()
        {
            keypadButton8.Click();
        }
        public void ClickOn_keypadButton9()
        {
            keypadButton9.Click();
        }

        public void ClickOn_keypadButtonAdd()
        {
            keypadButtonAdd.Click();
        }
        public void ClickOn_keypadButtonSub()
        {
            keypadButtonSub.Click();
        }
        public void ClickOn_keypadButtonMul()
        {
            keypadButtonMul.Click();
        }
        public void ClickOn_keypadButtonDiv()
        {
            keypadButtonDiv.Click();
        }

        public void ClickOn_keypadButtonPlusMinus()
        {
            keypadButtonPlusminus.Click();
        }

        public void ClickOn_keypadButtonDot()
        {
            keypadButtonDot.Click();
        }

        public void ClickOn_keypadParenthesis()
        {
            keypadButtonParenthesis.Click();
        }

        public void ClickOn_keypadPercentage()
        {
            keypadButtonPercentage.Click();
        }

        public void ClickOn_keypadСlear()
        {
            keypadButtonСlear.Click();
        }

        public void ClickOn_keypadDelete()
        {
            keypadButtonDelete.Click();
        }

        public void ClickOn_keypadEqual()
        {
            keypadButtonEqual.Click();
        }

    }
}
